package liam.weather.app.data.api

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
data class CityDataDto (
    var id: String? = null,
    var name: String? = null,
    var state: String? = null,
    var country: String? = null,
    var coord: Coordinations? = null
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class WeatherDto (
    var coord: Coordinations? = null,
    var weather: List<WeatherSummary>? = null,
    var main: WeatherDetails? = null,
    var wind: WindSummary? = null,
    var rain: RainSummary? = null,
    var clouds: CloudSummary? = null,
    var sys: SystemSummary? = null,
    var id: String? = null,
    var name: String? = null,
    var base: String? = null,
    var visibility: String? = null,
    var dt: String? = null
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class Coordinations (
    var lon: String? = null,
    var lat: String? = null
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class WeatherSummary (
    var id: Int? = null,
    var main: String? = null,
    var description: String? = null,
    var icon: String? = null
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class WeatherDetails (
    @JsonProperty("feels_like")
    var feelsLike: Double? = null,
    @JsonProperty("temp_min")
    var tempMin: Double? = null,
    @JsonProperty("temp_max")
    var tempMax: Double? = null,
    @JsonProperty("sea_level")
    var seaLevel: Int? = null,
    @JsonProperty("grnd_level")
    var grndLevel: Int? = null,
    var temp: Double? = null,
    var pressure: Int? = null,
    var humidity: Int? = null
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class WindSummary (
    var speed: Double? = null,
    var deg: Int? = null,
    var gust: Double? = null
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class RainSummary (
    @JsonProperty("1h")
    var oneHour: Double? = null
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class CloudSummary (
    var all: Int? = null
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class SystemSummary (
    var type: Int? = null,
    var id: Long? = null,
    var country: String? = null,
    var sunrise: Long? = null,
    var sunset: Long? = null
)