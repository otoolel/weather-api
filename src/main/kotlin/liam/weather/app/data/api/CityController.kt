package liam.weather.app.data.api

import liam.weather.app.data.service.CityService
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/city", produces = [MediaType.APPLICATION_JSON_VALUE])
class CityController(
    val cityService: CityService
) {
    @GetMapping
    fun get() : Collection<CityDataDto> {
        return cityService.getAll()
    }

    @GetMapping(value = ["filter"])
    fun filter(@RequestParam name: String) : Collection<CityDataDto> {
        return cityService.filterByName(name)
    }
}