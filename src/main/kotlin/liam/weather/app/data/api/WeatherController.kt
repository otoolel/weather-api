package liam.weather.app.data.api

import liam.weather.app.data.client.OpenWeatherClient
import liam.weather.app.data.service.CityService
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/weather", produces = [MediaType.APPLICATION_JSON_VALUE])
class WeatherController(
    val openWeatherClient: OpenWeatherClient
) {
    @GetMapping
    fun get(@RequestParam city: String) : WeatherDto? {
        return openWeatherClient.getWeatherForCity(city)
    }
}