package liam.weather.app.data.client

import com.fasterxml.jackson.core.type.TypeReference
import liam.weather.app.data.api.CityDataDto
import liam.weather.app.data.wrappers.ObjectMapperWrapper
import org.springframework.stereotype.Component

@Component
class CityClient(
    private val objectMapperWrapper: ObjectMapperWrapper
) {
    companion object {
        const val cityFile = "/city.list.json"
        val listCityDataType: TypeReference<Collection<CityDataDto>> = object : TypeReference<Collection<CityDataDto>>() {}
    }

    private var cityDataContent: Collection<CityDataDto> = emptyList()

    fun getCityData() : Collection<CityDataDto> {
        if (cityDataContent.isEmpty()) {
            loadCityData()
        }
        return cityDataContent
    }

    private fun loadCityData() {
        cityDataContent = objectMapperWrapper.readValueFromFile(cityFile, listCityDataType)
    }
}