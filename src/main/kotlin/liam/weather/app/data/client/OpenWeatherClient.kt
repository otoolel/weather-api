package liam.weather.app.data.client

import liam.weather.app.data.api.WeatherDto
import liam.weather.app.data.wrappers.RestTemplateWrapper
import org.apache.commons.lang3.StringUtils
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component

@Component
class OpenWeatherClient(
    private val restTemplateWrapper: RestTemplateWrapper
) {
    companion object {
        private const val weatherUri = "https://api.openweathermap.org/data/2.5/weather?id={CITY_ID}&appid={API_KEY}"
        private val weatherUriReplaceValues = arrayOf("{CITY_ID}", "{API_KEY}")
    }

    @Value("\${openweather.api.key}")
    private val apiKey: String? = null

    fun getWeatherForCity(cityId: String) : WeatherDto? {
        val uri = StringUtils.replaceEach(weatherUri, weatherUriReplaceValues, arrayOf(cityId, apiKey))
        return restTemplateWrapper.getForObject(uri, WeatherDto::class.java)
    }
}