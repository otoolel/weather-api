package liam.weather.app.data.service

import liam.weather.app.data.api.CityDataDto
import liam.weather.app.data.client.CityClient
import org.apache.commons.lang3.StringUtils
import org.springframework.stereotype.Service

@Service
class CityService(
    val cityClient: CityClient
) {
    fun getAll(): Collection<CityDataDto> {
        return cityClient.getCityData()
    }

    fun filterByName(name: String): Collection<CityDataDto> {
        return cityClient.getCityData().filter { StringUtils.equalsIgnoreCase(it.name, name) }
    }
}