package liam.weather.app.data.wrappers

import org.springframework.stereotype.Component

@Component
class ClassWrapper {
    fun getResourceAsStream(filePath: String) = this::class.java.getResourceAsStream(filePath)
}