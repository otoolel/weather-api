package liam.weather.app.data.wrappers

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import liam.weather.app.data.client.CityClient
import org.springframework.stereotype.Component
import java.io.InputStream

@Component
class ObjectMapperWrapper(
    private val objectMapper: ObjectMapper,
    private val classWrapper: ClassWrapper
) {

    fun <T> readValueFromFile(filePath: String, returnType: TypeReference<T>): T {
        val dataStream = classWrapper.getResourceAsStream(filePath)
        return readValueFromStream(dataStream, returnType)
    }

    fun <T> readValueFromStream(dataStream: InputStream, returnType: TypeReference<T>): T {
        return objectMapper.readValue(dataStream, returnType)
    }
}