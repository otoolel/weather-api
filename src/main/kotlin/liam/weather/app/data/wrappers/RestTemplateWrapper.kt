package liam.weather.app.data.wrappers

import org.springframework.stereotype.Component
import org.springframework.web.client.RestTemplate

@Component
class RestTemplateWrapper(
    private val restTemplate: RestTemplate
) {

    fun <T> getForObject(uri: String, responseType: Class<T>) : T? {
        return restTemplate.getForObject(uri, responseType)
    }
}