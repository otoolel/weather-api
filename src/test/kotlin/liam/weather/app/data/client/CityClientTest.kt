package liam.weather.app.data.client

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.verifyZeroInteractions
import liam.weather.app.data.api.CityDataDto
import liam.weather.app.data.wrappers.ObjectMapperWrapper
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.hasItems
import org.hamcrest.CoreMatchers.notNullValue
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.hasSize
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.ArgumentMatchers.any
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.junit.jupiter.MockitoExtension
import org.springframework.test.util.ReflectionTestUtils

@ExtendWith(MockitoExtension::class)
class CityClientTest {
    @Mock
    private lateinit var objectMapperWrapper: ObjectMapperWrapper
    @InjectMocks
    private lateinit var client: CityClient

    @Test
    fun `getCityData when data not loaded should load data from file and return it`() {
        // Arrange
        val clientData: Collection<CityDataDto> = mock()

        `when`(objectMapperWrapper.readValueFromFile(CityClient.cityFile, CityClient.listCityDataType)).thenReturn(clientData)
        
        // Act
        val result = client.getCityData()
        
        // Assert
        assertThat(result, `is`(clientData))
    }

    @Test
    fun `getCityData when data has been already loaded should not load data again`() {
        // Arrange
        val clientData: Collection<CityDataDto> = mock()

        ReflectionTestUtils.setField(client, "cityDataContent", clientData)

        `when`(clientData.isEmpty()).thenReturn(false)

        // Act
        val result1 = client.getCityData()
        val result2 = client.getCityData()

        // Assert
        assertThat(result1, `is`(clientData))
        assertThat(result2, `is`(clientData))
        verifyZeroInteractions(objectMapperWrapper)
    }
}