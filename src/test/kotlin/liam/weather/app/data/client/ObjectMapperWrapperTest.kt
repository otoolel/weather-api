package liam.weather.app.data.client

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.verifyZeroInteractions
import liam.weather.app.data.api.CityDataDto
import liam.weather.app.data.wrappers.ClassWrapper
import liam.weather.app.data.wrappers.ObjectMapperWrapper
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.hasItems
import org.hamcrest.CoreMatchers.notNullValue
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.hasSize
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.ArgumentMatchers.any
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Spy
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.junit.jupiter.MockitoExtension
import org.springframework.test.util.ReflectionTestUtils

@ExtendWith(MockitoExtension::class)
class ObjectMapperWrapperTest {
    companion object {
        val listStringType: TypeReference<List<String>> = object : TypeReference<List<String>>() {}
        val testObjectType: TypeReference<TestObject> = object : TypeReference<TestObject>() {}
    }

    @Spy
    private lateinit var objectMapperWrapper: ObjectMapper
    @Mock
    private lateinit var classWrapper: ClassWrapper
    @InjectMocks
    private lateinit var wrapper: ObjectMapperWrapper

    @Test
    fun `readValueFromFile should load data from file and return it as instance of required class`() {
        // Arrange
        val filePath = "a-file-path"
        val fileData: String = "[\"firstValue\", \"secondValue\", \"lastValue\"]"

        `when`(classWrapper.getResourceAsStream(filePath)).thenReturn(fileData.byteInputStream())
        
        // Act
        val result = wrapper.readValueFromFile(filePath, listStringType)
        
        // Assert
        assertThat(result, `is`(notNullValue()))
        assertThat(result, hasSize(3))
        assertThat(result, hasItems("firstValue", "secondValue", "lastValue"))
    }

    @Test
    fun `readValueFromStream should load data from stream and return it as instance of required class`() {
        // Arrange
        val data = "{\"id\": \"an-id-1\", \"name\": \"a-name-1\"}"

        // Act
        val result = wrapper.readValueFromStream(data.byteInputStream(), testObjectType)

        // Assert
        assertThat(result, `is`(notNullValue()))
        assertThat(result.id, `is`("an-id-1"))
        assertThat(result.name, `is`("a-name-1"))
    }
}

data class TestObject(
    var id: String? = null,
    var name: String? = null
)