package liam.weather.app.data.component

import com.fasterxml.jackson.databind.ObjectMapper
import com.mashape.unirest.http.HttpResponse
import com.mashape.unirest.http.Unirest
import liam.weather.app.data.DataApplication
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.TestPropertySource
import org.springframework.test.context.junit.jupiter.SpringExtension

@ExtendWith(SpringExtension::class)
@SpringBootTest(
    classes = [DataApplication::class],
    webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
@TestPropertySource("classpath:application-test.properties")
abstract class AbstractComponentTest() {

    @Autowired
    protected lateinit var objectMapper: ObjectMapper

    @Value("\${BASEURL}")
    protected lateinit var baseurl: String

    @Value("\${spring.security.user.name}")
    protected lateinit var userName: String

    @Value("\${spring.security.user.password}")
    protected lateinit var password: String

    fun get(resourceUrl: String): HttpResponse<String> {
        return Unirest.get("$baseurl$resourceUrl").basicAuth(userName, password).asString()
    }
}