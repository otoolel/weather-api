package liam.weather.app.data.component

import com.fasterxml.jackson.core.type.TypeReference
import liam.weather.app.data.api.CityDataDto
import org.apache.commons.lang3.StringUtils
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.notNullValue
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.emptyString
import org.hamcrest.Matchers.hasSize
import org.junit.jupiter.api.Test
import org.springframework.http.HttpStatus

class CityApiComponentTest: AbstractComponentTest() {
    companion object {
        val cityResourceUri = "/api/city"
        val filterResourceUri = "/filter?name="
        val listCityDataType: TypeReference<Collection<CityDataDto>> = object : TypeReference<Collection<CityDataDto>>() {}
    }

    @Test
    fun `get when cities exist should be returned`() {
        // Act
        val result = get(cityResourceUri)

        // Assert
        assertThat(result, `is`(notNullValue()))
        assertThat(result.status, `is`(HttpStatus.OK.value()))

        val resultCityData = objectMapper.readValue(result.body.byteInputStream(), listCityDataType)
        assertThat(resultCityData, hasSize(1315))

        val resultCorkCityData = resultCityData.first { StringUtils.equalsIgnoreCase(it.name, "Cork") }
        assertThat(resultCorkCityData, `is`(notNullValue()))
        assertThat(resultCorkCityData.id, `is`("2965140"))
        assertThat(resultCorkCityData.name, `is`("Cork"))
        assertThat(resultCorkCityData.state, `is`(emptyString()))
        assertThat(resultCorkCityData.country, `is`("IE"))
        assertThat(resultCorkCityData.coord?.lon, `is`("-8.47061"))
        assertThat(resultCorkCityData.coord?.lat, `is`("51.897968"))
    }

    @Test
    fun `filter when city exists for name should be returned`() {
        // Arrange
        val city = "Munster"
        val resourceUri = "$cityResourceUri$filterResourceUri$city"

        // Act
        val result = get(resourceUri)

        // Assert
        assertThat(result, `is`(notNullValue()))
        assertThat(result.status, `is`(HttpStatus.OK.value()))

        val resultCityData = objectMapper.readValue(result.body.byteInputStream(), listCityDataType)
        assertThat(resultCityData, hasSize(1))

        val resultFirst = resultCityData.first()
        assertThat(resultFirst, `is`(notNullValue()))
        assertThat(resultFirst.id, `is`("7521315"))
        assertThat(resultFirst.name, `is`("Munster"))
        assertThat(resultFirst.state, `is`(emptyString()))
        assertThat(resultFirst.country, `is`("IE"))
        assertThat(resultFirst.coord?.lon, `is`("-8.71667"))
        assertThat(resultFirst.coord?.lat, `is`("52.34333"))
    }
}