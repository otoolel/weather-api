package liam.weather.app.data.service

import com.nhaarman.mockito_kotlin.mock
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.`when`

import liam.weather.app.data.api.CityDataDto
import liam.weather.app.data.client.CityClient
import org.hamcrest.CoreMatchers.hasItems
import org.hamcrest.Matchers.emptyCollectionOf
import org.hamcrest.Matchers.hasSize
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.junit.jupiter.MockitoExtension

@ExtendWith(MockitoExtension::class)
class CityServiceTest {
    @Mock
    private lateinit var cityClient: CityClient

    @InjectMocks
    private lateinit var service: CityService

    @Test
    fun `getAll should return all cities`() {
        // Arrange
        val cityData: Collection<CityDataDto> = mock()

        `when`(cityClient.getCityData()).thenReturn(cityData)

        // Act
        val result = service.getAll()

        // Assert
        assertThat(result, `is`(cityData))
    }

    @Test
    fun `filterByName should return all cities that match a given name`() {
        // Arrange
        val monacoCity = CityDataDto(name = "Monaco")
        val monacoCity2 = CityDataDto(name = "monaco")

        val cityData: Collection<CityDataDto> =
            listOf(CityDataDto(name = "Dublin"), monacoCity, monacoCity2, CityDataDto(), CityDataDto(name = "Cork"))

        `when`(cityClient.getCityData()).thenReturn(cityData)

        // Act
        val result = service.filterByName("MONACO")

        // Assert
        assertThat(result, hasSize(2))
        assertThat(result, hasItems(monacoCity, monacoCity2))
    }

    @Test
    fun `filterByName when no city matches name should return empty`() {
        // Arrange
        val monacoCity = CityDataDto(name = "Monaco")
        val monacoCity2 = CityDataDto(name = "monaco")

        val cityData: Collection<CityDataDto> =
            listOf(CityDataDto(name = "Dublin"), monacoCity, monacoCity2, CityDataDto(), CityDataDto(name = "Cork"))

        `when`(cityClient.getCityData()).thenReturn(cityData)

        // Act
        val result = service.filterByName("unknown")

        // Assert
        assertThat(result, `is`(emptyCollectionOf(CityDataDto::class.java)))
    }
}