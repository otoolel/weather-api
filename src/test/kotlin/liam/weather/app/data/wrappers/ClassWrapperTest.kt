package liam.weather.app.data.wrappers

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.verifyZeroInteractions
import liam.weather.app.data.api.CityDataDto
import liam.weather.app.data.client.CityClient
import liam.weather.app.data.wrappers.ObjectMapperWrapper
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.hasItems
import org.hamcrest.CoreMatchers.instanceOf
import org.hamcrest.CoreMatchers.notNullValue
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.hasSize
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.ArgumentMatchers.any
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.junit.jupiter.MockitoExtension
import org.springframework.test.util.ReflectionTestUtils
import java.util.stream.Stream

class ClassWrapperTest {
    private val wrapper = ClassWrapper()

    @Test
    fun `getResourceAsStream should return a stream containing requested files content`() {
        // Arrange
        val filePath = "/TestFile.txt"

        // Act
        val result = wrapper.getResourceAsStream(filePath)
        
        // Assert
        assertThat(result, `is`(notNullValue()))
        val resultText = result.bufferedReader().use {
            it.readText()
        }
        assertThat(resultText, `is`("Some text - just for testing"))
    }
}